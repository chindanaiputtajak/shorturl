import { useEffect, useState } from "react";

const Table = ({ onClickEdit, onClickDelete }) => {
  const [url_list, setUrlList] = useState([]);

  const getAll = async () => {
    await fetch(process.env.API, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.status == 200) {
          //   console.log(res.data);
          setUrlList(res.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  return (
    <>
      <div className="container mx-auto px-4 sm:px-8">
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    URL
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Updated at
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Created at
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {url_list.map((item, index) => (
                  <tr key={index}>
                    <td className="px-5 py-5 border-b border-gray-200 w-[40%] bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        <a href={item.url} target="_blank">
                          {item.url}
                        </a>
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 w-[20%] bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {item.updated_at}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 w-[20%] bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {item.created_at}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 w-[20%] bg-white text-sm">
                      <span
                        className="cursor-pointer font-semibold bg-green-200 px-2 py-1 rounded-xl mr-1"
                        onClick={(event) =>
                          onClickEdit(true, item.key_id, item.url)
                        }
                      >
                        Edit
                      </span>
                      <span
                        className="cursor-pointer font-semibold bg-red-200 px-2 py-1 rounded-xl"
                        onClick={(event) =>
                          onClickDelete(true, item.key_id, item.url)
                        }
                      >
                        Delete
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default Table;
