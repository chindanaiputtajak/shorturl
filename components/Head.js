import Head from "next/head";
export default () => {
  return (
    <>
      <Head>
        <title>Short URL</title>
        <meta name="description" content="Short URL" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://fonts.googleapis.com/css?family=Lilita One"
          rel="stylesheet"
        />
      </Head>
    </>
  );
};
