import { useState, use } from "react";
import Result from "../components/frontend/result";
import { makeId, dataTimeNow } from "../util/methods";

export default function Home() {
  const [url, setUrl] = useState("");
  const [key_id, setKeyId] = useState("");
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const convert = async () => {
    setKeyId("");
    setLoading(true);
    const key_id = await makeId(5);
    if (url) {
      await fetch(process.env.API, {
        method: "POST",
        body: JSON.stringify({
          url: url,
          key_id: key_id,
          created_at: dataTimeNow(),
          updated_at: dataTimeNow(),
        }),
      })
        .then((res) => {
          if (res.status == 200) {
            setKeyId(key_id);
            setSuccess(true);
            setTimeout(() => {
              setSuccess(false);
            }, 3000);
          }
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
          console.error("Error:", error);
        });
    }
  };
  return (
    <>
      <div className="bg-[#FFDFD3] ">
        <div className="flex justify-center items-center h-screen ">
          <div className="bg-[#FEC8D8] p-4 rounded-md lg:w-[40%] md:w-[40%] xl:[40%] shadow sm:w-[90%]">
            <div className="bg-[#D291BC] w-[150px] rounded-full text-center p-2 -mt-11 -ml-2">
              <span className="text-white font-blod text-[30px]">
                Short URL
              </span>
            </div>
            <div className="grid gap-y-3">
              <div className="text-center text-[18px] text-gray-500 mt-2">
                Paste the URL to be shortened
              </div>
              <input
                type="text"
                onChange={(e) => setUrl(e.target.value)}
                placeholder="https://example.com"
                className="w-full text-[14px] focus:outline-none focus:rounded-sm focus:border-sky-500 focus:ring-1 focus:ring-sky-500 rounded-sm
             "
                name="input_url"
              />

              <div
                className={
                  success
                    ? "flex justify-between items-center"
                    : "flex justify-end items-center"
                }
              >
                {success && <div className="text-green-500">Successfully</div>}
                <div className="flex items-center gap-x-2">
                  {loading && (
                    <span className="animate-spin relative flex h-4 w-4 rounded-sm bg-purple-400 opacity-75"></span>
                  )}

                  <button
                    type="submit"
                    onClick={convert}
                    disabled={url ? false : true}
                    className={
                      url
                        ? `bg-[#E0BBE4] text-gray-700 cursor-pointer  p-2  rounded-full`
                        : `bg-gray-300 text-gray-400 cursor-no-drop  p-2  rounded-full`
                    }
                  >
                    SUBMIT
                  </button>
                </div>
              </div>
              {key_id && <Result key_id={key_id} />}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
