/** @type {import('next').NextConfig} */
const nextConfig = {
  async headers() {
    return [
      {
        source: "/_next/:path*",
        headers: [
          {
            key: "Access-Control-Allow-Origin",
            value: "https://shorturl-phi.vercel.app/",
          },
          {
            key: "Access-Control-Allow-Origin",
            value: "http://localhost:3000/",
          },
        ],
      },
    ];
  },
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: "https://shorturl-phi.vercel.app/:path*",
      },
      {
        source: "/api/:path*",
        destination: "http://localhost:3000/:path*",
      },
    ];
  },
  env: {
    API: "https://shorturl-phi.vercel.app/api/shorturl/",
    DOMAIN: "https://shorturl-phi.vercel.app/",
    // API: "http://localhost:3000/api/shorturl/",
    // DOMAIN: "http://localhost:3000/",
  },
  reactStrictMode: true,
};

module.exports = nextConfig;
