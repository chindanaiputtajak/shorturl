import { useState } from "react";

const Result = (props) => {
  const [copy_success, setCopySuccess] = useState(false);

  const onCopy = () => {
    navigator.clipboard.writeText(process.env.DOMAIN + props.key_id);

    setCopySuccess(true);
    setTimeout(() => {
      setCopySuccess(false);
    }, 3000);
  };
  return (
    <>
      <div>
        Your shortened URL
        <div className="text-[14px] ml-2 flex justify-between">
          <div>
            {/* <span className="text-[#957DAD]">
              {process.env.DOMAIN + props.key_id}
                      </span> */}

            <a
              href={process.env.DOMAIN + props.key_id}
              className="text-[#957DAD]"
              target="_blank"
            >
              {process.env.DOMAIN + props.key_id}
            </a>

            <span
              onClick={onCopy}
              className="text-white bg-sky-500 rounded-md px-1 ml-1 cursor-pointer"
            >
              copy
            </span>
          </div>
          {copy_success && <div className="text-green-500">Copy Success!</div>}
        </div>
      </div>
    </>
  );
};

export default Result;
