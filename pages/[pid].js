import { useRouter } from "next/router";
import { useEffect } from "react";

const shorturl = () => {
  const router = useRouter();
  const { pid } = router.query;

  const getUrl = async () => {
    await fetch(process.env.API + "?uid=" + pid, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        window.location.href = data.data.url;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    console.log(pid);
    if (!pid) {
      return;
    } else {
      getUrl();
    }
  }, [pid]);

  return (
    <>
      <div className="shadow rounded-md p-4 h-screen w-full ">
        <div className="animate-pulse flex space-x-4">
          <div className="flex-1 space-y-6 py-1">
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="space-y-3">
              <div className="grid grid-cols-3 gap-4">
                <div className="h-2 bg-slate-200 rounded col-span-2"></div>
                <div className="h-2 bg-slate-200 rounded col-span-1"></div>
              </div>
              <div className="h-2 bg-slate-200 rounded"></div>
              <div className="h-2 bg-slate-200 rounded"></div>
              <div className="grid grid-cols-4 gap-4">
                <div className="h-2 bg-slate-200 rounded col-span-2"></div>
                <div className="h-2 bg-slate-200 rounded col-span-2"></div>
              </div>
              <div className="h-2 bg-slate-200 rounded"></div>
              <div className="h-2 bg-slate-200 rounded w-[80%]"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default shorturl;
