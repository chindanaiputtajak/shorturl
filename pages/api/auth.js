import jwt from "jsonwebtoken";

const KEY = "yVQHWVyC6wIal6BeFwSr";

export default async function handler(req, res) {
  console.log("req.body", req.body);

  if (!req.body) {
    res.status = 500;
    res.end("Server Error");
  }
  const { username, password } = req.body;

  if (username === "admin" && password === "admin") {
    const token = {
      token: jwt.sign(
        {
          username,
          admin: username === "admin" && password === "admin",
        },
        KEY
      ),
    };

    res.json({
      statusCode: 200,
      data: token,
    });
  } else {
    res.json({
      statusCode: 400,
      msg: "Username or password not valid",
    });
  }
}
