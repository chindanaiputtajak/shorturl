import { useState, useEffect } from "react";
import jwt from "jsonwebtoken";
import Table from "../components/backend/table";
import { ModalEdit, ModalConfrimDelete } from "../components/backend/modal";

export default () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [check_login, setCheckLogin] = useState(false);
  const [error_msg, setErrorMsg] = useState("");
  const [modal_edit, setModalEdit] = useState({
    status: false,
    key_id: "",
    url: "",
  });
  const [modal_delete, setModalDelete] = useState({
    status: false,
    key_id: "",
    url: "",
  });

  useEffect(() => {
    const auth = JSON.parse(window.localStorage.getItem("Auth"));

    if (auth) {
      setCheckLogin(auth.admin.admin);
    }
  }, []);

  const onLogout = () => {
    localStorage.removeItem("Auth");
    window.location.reload(false);
  };

  const onSubmit = async () => {
    const res = await fetch("/api/auth", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        password,
      }),
    }).then((res) => res.json());

    if (res.statusCode == 200) {
      const token = res.data.token;

      if (token) {
        const json = jwt.decode(token);
        window.localStorage.setItem(
          "Auth",
          JSON.stringify({ token: token, admin: json })
        );
        setCheckLogin(true);
      }
    } else if (res.statusCode == 400) {
      setErrorMsg(res.msg);
    }
  };

  const onClickEdit = (status, key_id, url) => {
    setModalEdit({
      status,
      key_id,
      url,
    });
  };

  const onClickDelete = (status, key_id, url) => {
    setModalDelete({
      status,
      key_id,
      url,
    });
  };
  return (
    <>
      {modal_edit.status && (
        <ModalEdit data={modal_edit} onClickEdit={onClickEdit} />
      )}
      {modal_delete.status && (
        <ModalConfrimDelete data={modal_delete} onClickDelete={onClickDelete} />
      )}

      {check_login ? (
        <div className="bg-[#B3E3F8] h-screen flex justify-center ">
          <div className="w-[90%]  mt-4">
            <div className="flex justify-between items-center">
              <div className="text-[28px]">Short URL</div>{" "}
              <span
                onClick={onLogout}
                className="uppercase text-red-500 text-[20px] cursor-pointer"
              >
                Logout
              </span>
            </div>

            <Table onClickEdit={onClickEdit} onClickDelete={onClickDelete} />
          </div>
        </div>
      ) : (
        <div className="bg-[#B3E3F8] h-screen flex justify-center items-center">
          <div className="sm:w-[90%] md:w-[30%] bg-[#8CD1F6] rounded-lg p-2 border border-blue-300">
            <div className="text-white">
              <div className="text-[28px]">Short URL</div>
              <div className="text-[22px] text-center">Admin Login</div>
            </div>

            <div className="grid p-2 gap-4">
              <input
                type="text"
                name="username"
                onChange={(e) => setUsername(e.target.value)}
                className="w-full text-[14px] focus:outline-none focus:rounded-sm focus:border-sky-500 focus:ring-1 focus:ring-sky-500 rounded-sm"
              />
              <input
                type="password"
                name="password"
                onChange={(e) => setPassword(e.target.value)}
                className="w-full text-[14px] focus:outline-none focus:rounded-sm focus:border-sky-500 focus:ring-1 focus:ring-sky-500 rounded-sm"
              />
              {error_msg && <div className="text-red-800">* {error_msg}</div>}

              <button
                onClick={onSubmit}
                disabled={username && password ? false : true}
                className={
                  username && password
                    ? `bg-[#85BEF9] rounded-xl cursor-pointer text-white`
                    : `bg-gray-300 text-gray-400 cursor-no-drop  rounded-xl`
                }
              >
                LOGIN
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
