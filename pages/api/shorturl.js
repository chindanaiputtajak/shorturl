import clientPromise from "../../lib/mongodb";

export default async function handler(req, res) {
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Allow-Origin", "*");
  // another common pattern
  // res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,OPTIONS,PATCH,DELETE,POST,PUT"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
  );

  const client = await clientPromise;
  const db = client.db("shorturl");

  switch (req.method) {
    case "POST":
      let bodyObject = JSON.parse(req.body);
      let url_log = await db.collection("url_log").insertOne(bodyObject);
      res.json(url_log.ops[0]);
      break;

    case "GET":
      const { uid } = req.query;

      if (typeof uid != "undefined") {
        const url_data_by_param = await db
          .collection("url_log")
          .findOne({ key_id: uid });
        res.json({ status: 200, data: url_data_by_param });
      } else {
        const url_data = await db.collection("url_log").find({}).toArray();
        res.json({ status: 200, data: url_data });
      }
      break;

    case "PUT":
      let { key_id, url, updated_at } = JSON.parse(req.body);

      const res_update = await db
        .collection("url_log")
        .updateOne(
          { key_id: key_id },
          { $set: { url: url, updated_at: updated_at } }
        );
      res.json({ status: 200, data: res_update });
      break;

    case "DELETE":
      const { kid } = req.query;
      console.log(kid);

      const res_delete = await db
        .collection("url_log")
        .deleteOne({ key_id: kid });
      res.json({ status: 200, data: res_delete });
      break;
  }
}
