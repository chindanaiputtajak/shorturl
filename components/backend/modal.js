import { useEffect, useState } from "react";
import { dataTimeNow } from "../../util/methods";

export const ModalEdit = (props, { onClickEdit }) => {
  const [new_url, setNewUrl] = useState("");

  useEffect(() => {
    setNewUrl(props.data.url);
  }, [props]);

  const onUpdate = async () => {
    console.log(props);
    await fetch(process.env.API, {
      method: "PUT",
      body: JSON.stringify({
        url: new_url,
        key_id: props.data.key_id,
        updated_at: dataTimeNow(),
      }),
    })
      .then((res) => {
        if (res.status == 200) {
          window.location.reload(false);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };
  return (
    <>
      <div className="relative">
        <div className="z-20 w-[30%] center-screen shadow">
          <div className=" rounded-md bg-white w-full p-2 grid gap-y-2">
            <div className="text-center"> Edit URL </div>
            <hr />
            <input
              value={new_url}
              onChange={(e) => setNewUrl(e.target.value)}
              type="text"
              placeholder="URL"
              className="w-full border border-blue-500"
            />
            <div className="flex gap-2 justify-end ">
              <button
                onClick={(event) => props.onClickEdit(false, "", "")}
                className="bg-red-400  px-2 rounded-md"
              >
                Cancel
              </button>
              <button
                onClick={onUpdate}
                disabled={new_url ? false : true}
                className={
                  new_url
                    ? `bg-blue-400 px-2 rounded-md cursor-pointer`
                    : `bg-gray-300 text-gray-400 cursor-no-drop  p-2  rounded-md`
                }
              >
                OK
              </button>
            </div>
          </div>
        </div>
        <div className="absolute h-screen w-full bg-black opacity-40 z-10"></div>
      </div>
    </>
  );
};

export const ModalConfrimDelete = (props, { onClickDelete }) => {
  const onDelete = async () => {
    await fetch(process.env.API + "?kid=" + props.data.key_id, {
      method: "DELETE",
    })
      .then((res) => {
        if (res.status == 200) {
          window.location.reload(false);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <>
      <div className="relative">
        <div className="z-20 w-[30%] center-screen shadow">
          <div className=" rounded-md bg-white w-full p-2 grid gap-y-2">
            <div className="text-center"> Delete URL</div>
            <hr />
            Are you sure to delete?
            <div>URL : {props.data.url}</div>
            <div className="flex gap-2 justify-end">
              <button
                onClick={(event) => props.onClickDelete(false, "", "")}
                className="bg-red-400  px-2 rounded-md"
              >
                Cancel
              </button>
              <button
                className="bg-blue-400 px-2 rounded-md"
                onClick={onDelete}
              >
                OK
              </button>
            </div>
          </div>
        </div>
        <div className="absolute h-screen w-full bg-black opacity-40 z-10"></div>
      </div>
    </>
  );
};

export default ModalEdit;
